(ns advent2018.day9
  (:import java.util.LinkedList))

(def input "427 players; last marble is worth 70723 points"
  {:players 427 :marbles 70723})

(def input2 "427 players; last marble is worth 70723 points"
  {:players 427 :marbles 7072300})

(def sample "10 players; last marble is worth 1618 points"
  {:players 10 :marbles 1618})

(def sample2 {:players 9 :marbles 25})

;;TODO: Make this a lot faster :D
(defn solve [{:keys [players marbles]}]
  (let [board (LinkedList.)]
    (.add board 0)
    (loop [turn 0
           player-turn (map inc (cycle (range players)))
           marble (map inc (range marbles))
           current-index 0
           points {}]
      (if-let [next-marble (first marble)]
        (if (zero? (mod next-marble 23))
          (let [new-index (mod (- current-index 7) (.size board))
                extra-points (.get board new-index)
                current-player (first player-turn)]
            (.remove board (int new-index))
            (recur (inc turn)
                   (rest player-turn)
                   (rest marble)
                   new-index
                   (update points current-player
                           #(if %
                              (+ % next-marble extra-points)
                              (+ next-marble extra-points)))))
          (let [new-index (inc (mod (+ 1 current-index) (.size board)))]
            (.add board new-index next-marble)
            (recur (inc turn)
                   (rest player-turn)
                   (rest marble)
                   new-index
                   points)))
        (reduce max (vals points))))))
