(ns advent2018.day2
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))

(def input (str/split-lines (slurp (io/resource "day2.txt"))))

(defn solve [input]
  (let [dups (sequence (comp (map frequencies)
                             (map vals)
                             (map set))
                       input)
        [twos threes] (reduce (fn [[twos threes] dup]
                                [(if (dup 2) (inc twos) twos)
                                 (if (dup 3) (inc threes) threes)])
                              [0 0]
                              dups)]
    (* twos threes)))

(defn diff-val [a b]
  (reduce (fn [acc [a b]]
            (if (= a b) acc (inc acc)))
          0
          (map vector a b)))

(defn solve2 [input]
  (let [candidates (set (for [a input
                              b input
                              :when (= 1 (diff-val a b))]
                          [a b]))
        [a b] (first candidates)]
    (apply str (map first (filter (fn [[a b]] (= a b)) (map vector a b))))))
