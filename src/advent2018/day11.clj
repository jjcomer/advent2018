(ns advent2018.day11)

(def input 1133)

(def gen-power
  (memoize
   (fn [i [x y]]
     (let [rack-id (+ 10 x)]
       (-> rack-id
           (* y)
           (+ i)
           (* rack-id)
           (quot 100)
           (mod 10)
           (- 5))))))

;;TODO: Memoize and subdivide the answer for SPEEEEED
(defn sum-square [i s [x y]]
  (reduce + (for [x (range x (+ s x))
                  y (range y (+ s y))]
              (do
                #_(println [x y])
                (gen-power i [x y])))))

(defn solve [input size ss]
  (reduce (fn [[max coord :as old] new-coord]
            (let [new-value (m-sum-square input ss new-coord)]
              (if (> new-value max)
                [new-value new-coord]
                old)))
          (for [x (range 1 (- size (dec ss)))
                y (range 1 (- size (dec ss)))]
            [x y])))

(defn solve2 [input size]
  (reduce (fn [[max coord ss :as old] new-ss]
            (println new-ss old)
            (let [[value new-coord] (solve input size new-ss)]
              (if (> value max)
                [value new-coord new-ss]
                old)))
          [0 nil nil]
          (range 1 size)))
