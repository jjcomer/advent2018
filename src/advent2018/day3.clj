(ns advent2018.day3
  (:require [clojure.java.io :as io]
            [clojure.set :as set]
            [clojure.string :as str]))

(def sample "#1 @ 662,777: 18x27")

(defn parse [i]
  (let [[case _ size area :as all] (str/split i #" ")
        [x y] (str/split size #",")
        [height width] (map #(Integer. %) (str/split area #"x"))]
    {:case (Integer. (subs case 1))
     :x (Integer. x)
     :y (Integer. (subs y 0 (dec (count y))))
     :height height
     :width width}))

(def input (map parse
                (str/split-lines (slurp (io/resource "day3.txt")))))

(defn gen-range [start end]
  (range (max 0 (min 1000 (inc start)))
         (min 1001 (inc (+ start end)))))

(defn gen-cuts [patterns]
  (mapcat (fn [{:keys [x y height width]}]
            (for [x (gen-range x height)
                  y (gen-range y width)]
              [x y]))
          patterns))

(defn solve [input]
  (let [cuts (gen-cuts input)]
    (->> cuts
         frequencies
         vals
         (filter #(> % 1))
         count)))

;;Real gross, but didn't feel like writing out the fast pred :(
(defn solve2 [input]
  (let [patterns (set input)]
    (loop [i patterns]
      (when-let [n (first i)]
        (let [all-cuts (set (gen-cuts (disj patterns n)))
              this-cut (set (gen-cuts [n]))]
          (if (empty? (set/intersection all-cuts this-cut))
            (println (:case n))
            (recur (rest i))))))))
