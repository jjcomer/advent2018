(ns advent2018.day1
  (:require [clojure.edn :as edn]
            [clojure.java.io :as io]))

(def input (edn/read-string (slurp (io/resource "day1.edn"))))

(defn solve [i]
  (reduce + i))

(defn solve-2 [i]
  (loop [seen #{} current 0 i (cycle i)]
    (let [next (+ current (first i))]
      (if (seen next)
        next
        (recur (conj seen next) next (rest i))))))
