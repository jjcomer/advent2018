(ns advent2018.day8
  (:require [clojure.java.io :as io]
            [clojure.edn :as edn]))

(def input (edn/read-string (slurp (io/resource "day8.edn"))))
(def sample [ 2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2 ])

(defn get-metadata [tree meta sums]
  (let [[children-count meta-count & leftover] tree]
    (if (zero? children-count)
      [(drop meta-count leftover)
       (concat (take meta-count leftover) meta)
       (conj sums (reduce + (take meta-count leftover)))]
      (let [[tree meta csums] (reduce (fn [[tree meta sums] _]
                                        (get-metadata tree meta sums))
                                      [leftover meta []]
                                      (range children-count))
            this-meta (take meta-count tree)
            node-value (reduce (fn [acc n]
                                 (+ acc (nth csums (dec n) 0)))
                               0
                               this-meta)]
        [(drop meta-count tree)
         (concat this-meta meta)
         (conj sums node-value)]))))

(defn solve [input]
  (reduce + (second (get-metadata input [] []))))

(defn solve2 [input]
  (first (nth (get-metadata input [] []) 2)))
