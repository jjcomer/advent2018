(ns advent2018.day10
  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.edn :as edn]
            [quil.core :as q]))

(defn parse [x]
  (let [[_ position _ velocity] (str/split x #"[<>]")
        position (edn/read-string (str "[" position "]"))
        velocity (edn/read-string (str "[" velocity "]"))]
    {:position position
     :velocity velocity}))

(def sample (map parse (str/split-lines "position=< 9,  1> velocity=< 0,  2>
  position=< 7,  0> velocity=<-1,  0>
  position=< 3, -2> velocity=<-1,  1>
  position=< 6, 10> velocity=<-2, -1>
  position=< 2, -4> velocity=< 2,  2>
  position=<-6, 10> velocity=< 2, -2>
  position=< 1,  8> velocity=< 1, -1>
  position=< 1,  7> velocity=< 1,  0>
  position=<-3, 11> velocity=< 1, -2>
  position=< 7,  6> velocity=<-1, -1>
  position=<-2,  3> velocity=< 1,  0>
  position=<-4,  3> velocity=< 2,  0>
  position=<10, -3> velocity=<-1,  1>
  position=< 5, 11> velocity=< 1, -2>
  position=< 4,  7> velocity=< 0, -1>
  position=< 8, -2> velocity=< 0,  1>
  position=<15,  0> velocity=<-2,  0>
  position=< 1,  6> velocity=< 1,  0>
  position=< 8,  9> velocity=< 0, -1>
  position=< 3,  3> velocity=<-1,  1>
  position=< 0,  5> velocity=< 0, -1>
  position=<-2,  2> velocity=< 2,  0>
  position=< 5, -2> velocity=< 1,  2>
  position=< 1,  4> velocity=< 2,  1>
  position=<-2,  7> velocity=< 2, -2>
  position=< 3,  6> velocity=<-1, -1>
  position=< 5,  0> velocity=< 1,  0>
  position=<-6,  0> velocity=< 2,  0>
  position=< 5,  9> velocity=< 1, -2>
  position=<14,  7> velocity=<-2,  0>
  position=<-3,  6> velocity=< 2, -1>")))

(def input (map parse (str/split-lines (slurp (io/resource "day10.txt")))))

(defn update-point [p]
  (update p :position #(mapv + (:velocity p) %)))

(defn update-points [points]
  (-> points
      (update :points #(mapv update-point %))
      (update :frame inc)))

(defn draw [points]
  (q/background 255)
  (q/fill 0)
  (swap! points update-points)
  (let [{:keys [points frame]} @points]
    (q/text (str frame) 0 15)
    (doseq [point (map :position points)]
      (apply q/point point #_(map #(quot % 100) point)))))

(defn setup []
  (q/frame-rate 1)
  (q/background 255))

(defn print-range [points]
  (let [positions (map :position (:points points))
        x (map first positions)
        y (map second positions)]
    (println "X min " (reduce min x)
             "X max " (reduce max x)
             "Y min " (reduce min y)
             "Y max " (reduce max y))
    [(reduce max x) (reduce max y)]))

(defn solve
  ([input] (solve input 0))
  ([input skip]
   (let [skipped-points (reduce (fn [points _]
                                  (mapv update-point points))
                                input
                                (range skip))
         points (atom {:frame skip :points skipped-points})
         size (print-range @points)]
     (q/sketch
      :setup setup
      :size size
      :draw (partial draw points)))))
