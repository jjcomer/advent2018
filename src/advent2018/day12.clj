(ns advent2018.day12
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))

(defn to-bool [i]
  (mapv #(boolean (= \# %)) i))

(defn to-str [i]
  (apply str (map #(if % "#" ".") i)))

(defn parse [input]
  (let [[state _ & rules] (str/split-lines input)]
    {:state (to-bool (subs state 15))
     :rules (reduce (fn [acc rule-str]
                      (let [[r result] (str/split (str/trim rule-str) #" => ")]
                        (assoc acc
                               (to-bool r)
                               (boolean (= "#" result)))))
                    {}
                    rules)}))

(def sample (parse "initial state: #..#.#..##......###...###

  ...## => #
  ..#.. => #
  .#... => #
  .#.#. => #
  .#.## => #
  .##.. => #
  .#### => #
  #.#.# => #
  #.### => #
  ##.#. => #
  ##.## => #
  ###.. => #
  ###.# => #
  ####. => #"))

(def input (parse (slurp (io/resource "day12.txt"))))

(defn generate [state rules]
  ;;(println (count state))
  (reduce (fn [acc piece]
            ;;(println (to-str piece))
            (conj acc (boolean (get rules piece))))
          []
          (partition 5 1 (concat (repeat 5 false) state (repeat 5 false)))))

(defn solve [{:keys [rules state]} generations]
  (let [final (reduce (fn [state _]
                        (when (mod ))
                        (let [n (generate state rules)]
                          ;;(println (to-str n))
                          n))
                      state
                      (range generations))
        lowest-pot (* 3 generations)]
    {:lowest-pot lowest-pot
     :orig (count state)
     :final (count final)
     :result (transduce (comp (map-indexed (fn [i x] [(- i lowest-pot) x]))
                              (filter second)
                              (map #(do (println %) %))
                              (map first))
                        +
                        final)}))

(defn solve [{:keys [rules state]} generations]
  (let [final (reduce (fn [state _]
                        (let [n (generate state rules)]
                          ;;(println (to-str n))
                          n))
                      state
                      (range generations))
        lowest-pot (* 3 generations)]
    {:lowest-pot lowest-pot
     :orig (count state)
     :final (count final)
     :final-state final
     :result (transduce (comp (map-indexed (fn [i x] [(- i lowest-pot) x]))
                              (filter second)
                              #_(map #(do (println %) %))
                              (map first))
                        +
                        final)}))

(defn solve2 [input]
  (loop [gen (range 2000)
         last 0
         last-diff -1
         last-last-diff -2]
    (when-let [next-gen (first gen)]
      (println next-gen last-diff last-last-diff)
      (let [result (solve input next-gen)
            next (:result result)
            next-diff (- next last)]
        (if (= last-diff last-last-diff next-diff)
          {:gen next-gen
           :result (+ (* next-diff (- 50000000000 next-gen)) next)}
          (recur (rest gen)
                 next
                 next-diff
                 last-diff))))))
