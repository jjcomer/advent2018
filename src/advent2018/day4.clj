(ns advent2018.day4
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))

(def p-regex #"(\[.*\]) (.*)")

(defn date-split [i]
  (->> i
       (re-seq p-regex)
       first
       rest))

(def guard-regex #"^Guard #(.*) begins shift$")

(defn find-guard-id [[_ id]]
  (Integer. (second (re-matches guard-regex id))))

(defn get-shift [work]
  (take-while (fn [[_ action]] (not (str/starts-with? action "G"))) work))

(defn gen-work-blocks [work]
  (loop [workers {} work work]
    (if (seq work)
      (let [id (find-guard-id (first work))
            shift (get-shift (rest work))]
        (recur (update workers id #(if % (conj % shift) [shift]))
               (drop (inc (count shift)) work)))
      workers)))

(defn parse [input]
  (->> input
       (map date-split)
       (sort-by first)
       gen-work-blocks))

(def input (->> "day4.txt"
                io/resource
                slurp
                str/split-lines
                parse))

(defn get-date [s] (subs s 6 11))
(defn get-minutes [s]

  (Integer. (subs s 15 17)))

(defn gen-sleep [d1 d2]
  (range (get-minutes d1) (get-minutes d2)))

(defn top-minute [freq]
  (reduce (fn [[_ f1 :as top] [_ f2 :as x]]
            (if (> f2 f1)
              x top))
          [0 0]
          freq))

(defn solve [input]
  (let [ranges (reduce-kv (fn [acc guard schedules]
                            (let [sleep (flatten (mapcat (fn [schedule]
                                                           (map (fn [[[d1 _] [d2 _]]]
                                                                  (gen-sleep d1 d2))
                                                                (partition 2 schedule)))
                                                         schedules))]
                              (conj acc {:guard guard
                                         :sleep sleep
                                         :total (count sleep)
                                         :freq (frequencies sleep)})))
                          []
                          input)
        top (reduce (fn [top {:keys [sleep total] :as g}]
                      (if (> total (:total top 0))
                        g
                        top))
                    nil
                    ranges)
        minute (reduce (fn [[_ f1 :as top] [_ f2 :as x]]
                         (if (> f2 f1)
                           x top)) [0 0]
                       (:freq top))
        top-freq (reduce (fn [[_ f1 :as top] [_ f2 :as x]]
                           (if (> f2 f1)
                             x top)) (apply merge-with + (map :freq ranges)))
        top-sleeper (reduce (fn [top-g x]
                              (if (> (second (top-minute (:freq x)))
                                     (second (top-minute (:freq top-g))))
                                x top-g))
                            ranges)
        top-minute (top-minute (:freq top-sleeper))]
    (assoc top :minute minute
           :answer (* (first minute) (:guard top))
           :top-sleeper (:guard top-sleeper)
           :top-minute top-minute
           :top-answer (* (:guard top-sleeper) (first top-minute)))))
