(ns advent2018.day7
  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.set :as set]))

(def sample "Step C must be finished before step A can begin.")

(defn parse [s]
  [(subs s 5 6)
   (subs s 36 37)])

(def input (mapv parse (str/split-lines (slurp (io/resource "day7.txt")))))

(defn nodes [input]
  (reduce (fn [acc [from to]]
            (update acc to conj from))
          (reduce (fn [acc n] (assoc acc n #{}))
                  {}
                  (set (flatten input)))
          input))

(defn next-work
  [graph]
  (sort (reduce-kv (fn [acc k v]
                     (if (empty? v)
                       (conj acc k)
                       acc))
                   []
                   graph)))

(defn remove-work
  [graph target]
  (reduce-kv (fn [acc k v]
               (assoc acc k (disj v target)))
             {}
             (dissoc graph target)))

(defn update-graph
  [graph]
  (let [target (first (next-work graph))]
    [target
     (remove-work graph target)]))

(defn solve [input]
  (let [graph (nodes input)]
    (loop [order [] graph graph]
      (if (seq graph)
        (let [[n graph] (update-graph graph)]
          (recur (conj order n) graph))
        (apply str order)))))

(defn time-needed [x] (- (int (first x)) 4))

;;;;TODO: Account for work completion

(defn boost-workers [w graph]
  (let [nw (doall (filter #(pos? (second %)) w))
        dworkers (doall (map first (filter #(zero? (second %)) w)))]
    (loop [w nw graph (reduce remove-work graph dworkers)]
      (if (< (count w) 5)
        (if (seq graph)
          (let [current-work (set (map first w))
                target (first (remove current-work (next-work graph)))]
            (if target
              (recur (conj w [target (time-needed target)])
                     graph)
              [w graph]))
          [w graph])
        [w graph]))))

(defn solve2 [input]
  (let [graph (nodes input)]
    (loop [workers [] graph graph time 0]
      (if (seq graph)
        (let [[workers graph] (boost-workers workers graph)]
          (recur (map #(update % 1 dec) workers)
                 graph
                 (inc time)))
        (if (seq workers)
          (recur (map #(update % 1 dec) (filter #(pos? (second %)) workers))
                 nil
                 (inc time))
          (dec time))))))
