(ns advent2018.day6
  (:require [clojure.java.io :as io]
            [clojure.edn :as edn]))

(def sample (partition 2 [1, 1 1, 6 8, 3 3, 4 5, 5 8, 9]))
(def input (partition 2 (edn/read-string (slurp (io/resource "day6.edn")))))

(defn grid-size [i]
  [(reduce max (map first i))
   (reduce max (map second i))])

(defn find-closest [points x y]
  (first (reduce (fn [[min-point min-distance] [point [px py]]]
                   (let [distance (+ (Math/abs (- px x))
                                     (Math/abs (- py y)))]
                     (cond
                       (= distance min-distance) [:tie min-distance]
                       (< distance min-distance) [point distance]
                       :else [min-point min-distance])))
                 [:none Integer/MAX_VALUE]
                 (map-indexed vector points))))

(defn find-total-distance [points x y]
  (reduce (fn [distance [px py]]
            (+ distance (+ (Math/abs (- px x))
                           (Math/abs (- py y)))))
          0
          points))

(defn find-edges [grid max-x max-y]
  (reduce-kv (fn [acc [x y] v]
               (cond
                 (= :tie v) acc
                 (or (= 0 x) (= 0 y)) (conj acc v)
                 (or (= max-x x) (= max-y y)) (conj acc v)
                 :else acc))
             #{}
             grid))

(defn solve [input]
  (let [[max-x max-y] (grid-size input)
        grid (into {} (for [x (range (inc max-x))
                            y (range (inc max-y))]
                        [[x y] (find-closest input x y)]))
        edges (find-edges grid max-x max-y)]
    (last (sort (vals (dissoc (frequencies (remove edges (vals grid))) :tie))))))

(defn solve2 [input]
  (let [[max-x max-y] (grid-size input)
        grid (for [x (range (inc max-x))
                   y (range (inc max-y))]
               (find-total-distance input x y))]
    (count (filter #(< % 10000) grid))))
