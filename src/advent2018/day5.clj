(ns advent2018.day5
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))

(def sample "dabAcCaCBAcCcaDA")

(def input (str/trim (slurp (io/resource "day5.txt"))))

(defn nuke? [[a b]]
  (if (nil? b)
    false
    (and (not= a b)
         (= (str/lower-case a) (str/lower-case b)))))

(defn clean-string [s]
  (filter identity
          (flatten (sequence (comp (remove nuke?))
                             (partition 2 2 (repeat nil) s)))))

(defn solve [input]
  (loop [x (seq input)]
    (let [result (clean-string x)
          result (cons (first result) (clean-string (rest result)))]
      (if (= (count x) (count result))
        (count result)
        (recur result)))))

(defn solve2 [input]
  (let [cases (map set (partition 2 "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz"))]
    (reduce min (pmap solve (pmap #(remove % input) cases)))))
